# How to deploy
```
npm install
sls deploy
```

# How to use
* set needed topic according to docs:
https://digitransit.fi/en/developers/apis/4-realtime-api/vehicle-positions/
* press **Topic submit** button
* press **Track start** button to start fetching new positions from last 2 minutes

# Live example
`https://oeriibk2ye.execute-api.eu-west-1.amazonaws.com/dev/app`

# Known issues
* Clicking "Topic submit" each time will cause creating separate mqtt listener inside lambda.
There is no check if current topic is already listened by lambda.

If I had more time I would
add second table named "handlers" into DynamoDB that will have 2 columns: topicTemplate(primary) and handler.
Each time function positionsTrack is executed it would insert/update record in handlers table: will set 
topicTemplate and handlerid (for example uuid).
Function positionsTrack also should react on event when row in handlers table will be changed for topicTemplate
so function will interrupt immediately.

## P.S.
I did not have previous experience with Lambda, DynamoDB, serverless framework - so I had to learn all these
things during my work. Unfortunately it eat significant part of my time (especially DynamoDB learning)
so I just did not have enough time to cover all cases. I understand that not all my solutions are optimal. 
