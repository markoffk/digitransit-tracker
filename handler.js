'use strict';

const mqtt = require('mqtt');
const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const positionsTableName = 'positions';

module.exports.render = async event => {
    const html = `
        <html>
        <script>
            let controller, signal;
            let interval;
            let fetching = false;
            
            function fetchPositions() {
                const topicTemplate = encodeURIComponent(document.querySelector('#inputTopicTemplate').value);

                fetch('/dev/positions?topicTemplate=' + topicTemplate, {signal}).then(function (response) {
                    response.json().then(json => {
                        const minLat = Math.min(...json.map(item => item.lat));
                        const minLong = Math.min(...json.map(item => item.long));

                        const points = json.map(item => {
                            return (item.lat - minLat) * 1000 + '   ' + (item.long - minLong) * 1000;
                        }).join('<br>');
                        
                        document.querySelector('#route').innerHTML = points;
                        
                        if (fetching) {
                            setTimeout(fetchPositions, 1000);
                        }
                    });
                })
            }

            function handleTopicSubmit(event) {
                fetch('/dev/position-tracking', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ topicTemplate: document.querySelector('#inputTopicTemplate').value })
                })
            }

            function handleTrackStart(event) {
                fetching = true;
                document.querySelector('#btnTrackStart').disabled = true;
                document.querySelector('#btnTrackStop').disabled = false;
                
                controller = new AbortController();
                signal = controller.signal;
                
                fetchPositions();
            }

            function handleTrackStop(event) {
                fetching = false;
                document.querySelector('#btnTrackStart').disabled = false;
                document.querySelector('#btnTrackStop').disabled = true;

                controller.abort();
                clearInterval(interval);
            }

            window.onload = function () {
                const btnTopicSubmit = document.querySelector("#btnTopicSubmit");
                btnTopicSubmit.addEventListener('click', handleTopicSubmit);

                const btnTrackStart = document.querySelector("#btnTrackStart");
                btnTrackStart.addEventListener('click', handleTrackStart);

                const btnTrackStop = document.querySelector("#btnTrackStop");
                btnTrackStop.addEventListener('click', handleTrackStop);
            };
        </script>
        <body>
        <h1>Digitransit vechicle tracker</h1>
        <input type="text" id="inputTopicTemplate" size="50" value="/hfp/v2/journey/+/vp/bus/+/+/2239/+/+/+/+/+/#" />
        <button id="btnTopicSubmit">Topic submit</button>
        <br>
        <hr>
        <button id="btnTrackStart">Track start</button>
        <button id="btnTrackStop">Track cancel</button>
        
        <br>
        Route points:
        <div id="route"></div>

        </body>
        </html>
    `;

    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html',
        },
        body: html,
    };
};

module.exports.positionsTrack = (event, context, callback) => {
    const client = mqtt.connect('mqtts://mqtt.hsl.fi:8883');
    const submittedData = JSON.parse(event.body);
    const topicTemplate = submittedData.topicTemplate || '';

    const params = {
        TableName: positionsTableName,
    };
    client.on('connect', function () {
        client.subscribe(topicTemplate, function (err) {})
    });

    client.on('message', function (realTopic, message) {
        const item = JSON.parse(message.toString());

        docClient.put({
            ...params,
            Item: {
                topicTemplate: topicTemplate,
                eventTimestamp: Date.now(),
                topic: realTopic,
                data: item,
            },
        }, function (err, data) {
        });

    });
};

module.exports.positionsGet = (event, context, callback) => {
    let topicTemplate = '';
    if (event.queryStringParameters && event.queryStringParameters.topicTemplate) {
        topicTemplate = event.queryStringParameters.topicTemplate;
    }

    const params = {
        TableName: positionsTableName,
        KeyConditionExpression: 'topicTemplate = :v1 and eventTimestamp > :t1',
        ExpressionAttributeValues: {
            ':v1': topicTemplate,
            ':t1': Date.now() - 1000 * 60 * 2,
        },
        ScanIndexForward: false,
    };
    docClient.query(params, (error, result) => {
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' },
                body: `Couldn't fetch positions. ${error}`,
            });
            return;
        }

        const response = {
            statusCode: 200,
            body: JSON.stringify(result.Items.map(item => {
                return {
                    tsi: item.data.VP.tsi,
                    lat: item.data.VP.lat,
                    long: item.data.VP.long,
                }
            })),
        };
        callback(null, response);
    });
};
